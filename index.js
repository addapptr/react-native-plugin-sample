/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import ReuseBannerScreen from './ReuseBannerScreen'
import ReuseBannerScreen2 from './ReuseBannerScreen2';
import MRECBanner from './MRECBanner'
import InFeed from './InFeed';
import {name as appName} from './app.json';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const MainNavigator = createStackNavigator({
    Home: {screen: App},
    ReuseBannerScreen: {screen : ReuseBannerScreen},
    ReuseBannerScreen2: {screen: ReuseBannerScreen2},
    MRECBanner: {screen: MRECBanner},
    InFeed: {screen: InFeed}
  });

const MainApp = createAppContainer(MainNavigator);

AppRegistry.registerComponent(appName, () => MainApp);
